# bayesAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import bayesNet as bn
import game
from game import Actions, Agent, Directions
import inference
import layout
import factorOperations
import itertools
import operator as op
import random
import functools
import util

from hunters import GHOST_COLLISION_REWARD, WON_GAME_REWARD
from layout import PROB_BOTH_TOP, PROB_BOTH_BOTTOM, PROB_ONLY_LEFT_TOP, \
    PROB_ONLY_LEFT_BOTTOM, PROB_FOOD_RED, PROB_GHOST_RED

X_POS_VAR = "xPos"
FOOD_LEFT_VAL = "foodLeft"
GHOST_LEFT_VAL = "ghostLeft"
X_POS_VALS = [FOOD_LEFT_VAL, GHOST_LEFT_VAL]

Y_POS_VAR = "yPos"
BOTH_TOP_VAL = "bothTop"
BOTH_BOTTOM_VAL = "bothBottom"
LEFT_TOP_VAL = "leftTop"
LEFT_BOTTOM_VAL = "leftBottom"
Y_POS_VALS = [BOTH_TOP_VAL, BOTH_BOTTOM_VAL, LEFT_TOP_VAL, LEFT_BOTTOM_VAL]

FOOD_HOUSE_VAR = "foodHouse"
GHOST_HOUSE_VAR = "ghostHouse"
HOUSE_VARS = [FOOD_HOUSE_VAR, GHOST_HOUSE_VAR]

TOP_LEFT_VAL = "topLeft"
TOP_RIGHT_VAL = "topRight"
BOTTOM_LEFT_VAL = "bottomLeft"
BOTTOM_RIGHT_VAL = "bottomRight"
HOUSE_VALS = [TOP_LEFT_VAL, TOP_RIGHT_VAL, BOTTOM_LEFT_VAL, BOTTOM_RIGHT_VAL]

OBS_VAR_TEMPLATE = "obs(%d,%d)"

BLUE_OBS_VAL = "blue"
RED_OBS_VAL = "red"
NO_OBS_VAL = "none"
OBS_VALS = [BLUE_OBS_VAL, RED_OBS_VAL, NO_OBS_VAL]

ENTER_LEFT = 0
ENTER_RIGHT = 1
EXPLORE = 2

def constructHeartAttackBayesNet():


    # Las variables son: Exercise, Smokes, Chol, BP, Attack
    variableList = ['Exercise', 'Smokes', 'Chol', 'BP', 'Attack']

    #Constructing Bayes' nets, edge list: (x, y) means edge from x to y.

    # Las aristas son:  (Exercise, BP), (Smokes, BP), (Smokes, Chol), (BP, Attack)
    edgeTuplesList = [('Exercise', 'BP'), ('Smokes', 'BP'), ('Smokes', 'Chol'), ('BP', 'Attack')]

    # Construct the domain for each variable (a list like)

    # Los dominios son: Exercise = {T, F}, Smokes = {T, F}, Chol = {T, F}, BP = {T, F}, Attack = {T, F}
    variableDomainsDict = {}
    variableDomainsDict['Exercise']  = ['T', 'F']
    variableDomainsDict['Smokes'] = ['T', 'F']
    variableDomainsDict['Chol']  = ['T', 'F']
    variableDomainsDict['BP']  = ['T', 'F']
    variableDomainsDict['Attack']  = ['T', 'F']

    # None of the conditional probability tables are assigned yet in our Bayes' net

    # Construye una red bayesiana vacía
    bayesNet = bn.constructEmptyBayesNet(variableList, edgeTuplesList, variableDomainsDict)

    # Create a factor for each CPT.  
    # The first input is the list of unconditioned variables in your factor,
    # the second input is the list of conditioned variables in your factor,
    # and the third input is the dict of domains for your variables.

    # Construye una tabla de probabilidad condicional para Exercise
    exerciseCPT  = bn.Factor(['Exercise'], [], variableDomainsDict)


    # Establece la probabilidad de Exercice
    exerAssignmentDict = {'Exercise' : 'T'}
    exerciseCPT.setProbability(exerAssignmentDict, 0.4)

    # Establece la probabilidad de Exercice
    exerAssignmentDict = {'Exercise' : 'F'}
    # Establece la probabilidad de  no Exercice
    exerciseCPT.setProbability(exerAssignmentDict, 0.6)

    print(exerciseCPT)





    smokesCPT  = bn.Factor(['Smokes'], [], variableDomainsDict)

    # Establece la probabilidad de Smokes
    smokesAssignmentDict = {'Smokes' : 'T'}
    smokesCPT.setProbability(smokesAssignmentDict, 0.15)

    # Establece la probabilidad de  no Smokes
    smokesAssignmentDict = {'Smokes' : 'F'}
    smokesCPT.setProbability(smokesAssignmentDict, 0.85)

    print(smokesCPT)




    # Construye una tabla de probabilidad condicional para el cholestrol
    CholCPT  = bn.Factor(['Chol'], ['Smokes'], variableDomainsDict)

    # Establecemos las posibilidades de True y False para el cholestrol
    SC = {'Smokes' : 'T', 'Chol' : 'T'}
    Sc = {'Smokes' : 'F',  'Chol' : 'T'}
    sC = {'Smokes' : 'T', 'Chol' : 'F'}
    sc = {'Smokes' : 'F',  'Chol' : 'F'}

    # Establecemos las posibilidades de True y False para el cholestrol
    CholCPT.setProbability(SC, 0.8)
    CholCPT.setProbability(Sc, 0.2)
    CholCPT.setProbability(sC, 0.4)
    CholCPT.setProbability(sc, 0.6)

    print(CholCPT)



    # Construye una tabla de probabilidad condicional para el BP
    bpCPT  = bn.Factor(['BP'], ['Smokes', 'Exercise'], variableDomainsDict)

    # Establecemos las posibilidades de True y False para el BP
    ESc = {'BP' : 'T', 'Smokes' : 'T', 'Exercise' : 'T'}
    ESb = {'BP' : 'T',  'Smokes' : 'T', 'Exercise' : 'F'}
    EsB = {'BP' : 'T', 'Smokes' : 'F',  'Exercise' : 'T'}
    Esb = {'BP' : 'T',  'Smokes' : 'F',  'Exercise' : 'F'}
    eSB = {'BP' : 'F', 'Smokes' : 'T', 'Exercise' : 'T' }
    eSb = {'BP' : 'F',  'Smokes' : 'T', 'Exercise' : 'F' }
    esB = {'BP' : 'F', 'Smokes' : 'F',  'Exercise' : 'T' }
    esb = {'BP' : 'F',  'Smokes' : 'F',  'Exercise' : 'F' }


    # Establecemos las posibilidades de True y False para el BP
    bpCPT.setProbability(ESc, 0.45)
    bpCPT.setProbability(ESb, 0.55)
    bpCPT.setProbability(EsB, 0.05)
    bpCPT.setProbability(Esb, 0.95)
    bpCPT.setProbability(eSB, 0.95)
    bpCPT.setProbability(eSb, 0.05)
    bpCPT.setProbability(esB, 0.55)
    bpCPT.setProbability(esb, 0.45)

    print(bpCPT)



    # Construye una tabla de probabilidad condicional para A
    AttackCPT  = bn.Factor(['Attack'], ['BP'], variableDomainsDict)

    # Establecemos las posibilidades de True y False para Attack
    BA = {'BP' : 'T', 'Attack' : 'T'}
    Ba = {'BP' : 'F',  'Attack' : 'T'}
    bA = {'BP' : 'T', 'Attack' : 'F'}
    ba = {'BP' : 'F',  'Attack' : 'F'}

    # Establecemos las posibilidades de True y False para Attack
    AttackCPT.setProbability(BA, 0.75)
    AttackCPT.setProbability(Ba, 0.25)
    AttackCPT.setProbability(bA, 0.05)  
    AttackCPT.setProbability(ba, 0.95)

    print(AttackCPT)

    
    # Se agregan las tablas de probabilidad condicional a la red bayesiana
    for assignmentDict in AttackCPT.getAllPossibleAssignmentDicts():
        print(assignmentDict)

    # Se agregan las tablas de probabilidad condicional a la red bayesiana
    bayesNet.setCPT('Exercise', exerciseCPT)
    bayesNet.setCPT('Smokes', smokesCPT)
    bayesNet.setCPT('Chol', CholCPT)
    bayesNet.setCPT('BP', bpCPT)

    print(bayesNet)

    return bayesNet

    print("Print a Bayes' net to see its variables, edges, and " + \
          "the CPT for each variable.\n")
    print(bayesNet)

    print("You can get a list of all CPTs from a Bayes' net, instantiated with " + \
          "evidence, with the getAllCPTsWithEvidence function. " + \
          "The evidenceDict input is an assignmentDict of " + \
          "(evidenceVariable, evidenceValue) pairs. " + \
          "Instantiation with evidence reduces the variable domains and thus " + \
          "selects a subset of entries from the probability table.")

    evidenceDict = {'Raining' : 'T'}
    for CPT in bayesNet.getAllCPTsWithEvidence(evidenceDict):
        print(CPT)

    print('If it is empty or None, the full CPTs will be returned. \n')

    for CPT in bayesNet.getAllCPTsWithEvidence():
        print(CPT)

    print("If only one variable's CPT is desired, you can get just that particular " + \
          "CPT with the bayesNet.getCPT function. \n")

    print(bayesNet.getCPT('Traffic'))

    print(bayesNet.easierToParseString())

def constructBayesNet(gameState):
    """
    Question 1: Bayes net structure

    Construct an empty Bayes net according to the structure given in the project
    description.

    There are 5 kinds of variables in this Bayes net:
    - a single "x position" variable (controlling the x pos of the houses)
    - a single "y position" variable (controlling the y pos of the houses)
    - a single "food house" variable (containing the house centers)
    - a single "ghost house" variable (containing the house centers)
    - a large number of "observation" variables for each cell Pacman can measure

    You *must* name all position and house variables using the constants
    (X_POS_VAR, FOOD_HOUSE_VAR, etc.) at the top of this file. 

    The full set of observation variables can be obtained as follows:

        for housePos in gameState.getPossibleHouses():
            for obsPos in gameState.getHouseWalls(housePos)
                obsVar = OBS_VAR_TEMPLATE % obsPos

    In this method, you should:
    - populate `obsVars` using the procedure above
    - populate `edges` with every edge in the Bayes Net (a tuple `(from, to)`)
    - set each `variableDomainsDict[var] = values`, where `values` is the set
      of possible assignments to `var`. These should again be set using the
      constants defined at the top of this file.
    """

    # En obsVars se guardan las variables observables
    obsVars = []
    # En edges se guardan las aristas de la red bayesiana
    edges = []
    # En variableDomainsDict se guardan los dominios de las variables
    variableDomainsDict = {}

    "*** YOUR CODE HERE ***"
    # Con este for se obtienen las posiciones de las casas
    for house in HOUSE_VARS:
        edges.append((X_POS_VAR, house))
        edges.append((Y_POS_VAR, house))

    # Para cada casa se obtienen las posiciones de las paredes de la casa y se agregan a obsVars 
    for housePos in gameState.getPossibleHouses():
        for obsPos in gameState.getHouseWalls(housePos):
            # Se obtiene el nombre de la variable observable, ya que con OBS_VAR_TEMPLATE % obsPos se obtiene el nombre de la variable observable
            obsVar = OBS_VAR_TEMPLATE % obsPos
            obsVars.append(obsVar)
            # Se agregan las aristas de las casas con las variables observables
            for house in HOUSE_VARS:
                edges.append((house, obsVar))

    # Se agregan las aristas de las casas con los observables 
    variableDomainsDict[X_POS_VAR] = X_POS_VALS
    variableDomainsDict[Y_POS_VAR] = Y_POS_VALS
    # Se agregan los dominios de las casas y los observables 
    for house in HOUSE_VARS:
        variableDomainsDict[house] = HOUSE_VALS
    for obsVar in obsVars:
        variableDomainsDict[obsVar] = OBS_VALS

    # Se agregan las aristas de los observables con los observables 
    variables = [X_POS_VAR, Y_POS_VAR] + HOUSE_VARS + obsVars
    net = bn.constructEmptyBayesNet(variables, edges, variableDomainsDict)

    # Devolvemos la red y los observables 
    return net, obsVars

# Estas variables son para el pacman, para que sepa donde esta la comida y los fantasmas
def fillCPTs(bayesNet, gameState):
    fillXCPT(bayesNet, gameState)
    fillYCPT(bayesNet, gameState)
    fillHouseCPT(bayesNet, gameState)
    fillObsCPT(bayesNet, gameState)

# Se obtiene la distancia entre dos puntos
def fillXCPT(bayesNet, gameState):
    from layout import PROB_FOOD_LEFT
    # Se crea la tabla de probabilidad condicional para la variable x, que es la posicion en x de las casas
    xFactor = bn.Factor([X_POS_VAR], [], bayesNet.variableDomainsDict())

    # Se establece la probabilidad de que la comida este a la izquierda
    xFactor.setProbability({X_POS_VAR: FOOD_LEFT_VAL}, PROB_FOOD_LEFT)
    # Se establece la probabilidad de que la comida este a la derecha
    xFactor.setProbability({X_POS_VAR: GHOST_LEFT_VAL}, 1 - PROB_FOOD_LEFT)
    # Se agrega la tabla de probabilidad condicional a la red bayesiana
    bayesNet.setCPT(X_POS_VAR, xFactor)

def fillYCPT(bayesNet, gameState):
    """
    Question 2a: Bayes net probabilities

    Fill the CPT that gives the prior probability over the y position variable.
    See the definition of `fillXCPT` above for an example of how to do this.
    You can use the PROB_* constants imported from layout rather than writing
    probabilities down by hand.
    """
    from layout import PROB_BOTH_TOP, PROB_BOTH_BOTTOM, PROB_ONLY_LEFT_TOP, PROB_ONLY_LEFT_BOTTOM

    # Se crea la tabla de probabilidad condicional para la variable y, que es la posicion en y de las casas
    yFactor = bn.Factor([Y_POS_VAR], [], bayesNet.variableDomainsDict())
    # Se establece la probabilidad de que la comida este arriba
    yFactor.setProbability({Y_POS_VAR: BOTH_TOP_VAL}, PROB_BOTH_TOP)
    # Se establece la probabilidad de que la comida este abajo
    yFactor.setProbability({Y_POS_VAR: BOTH_BOTTOM_VAL}, PROB_BOTH_BOTTOM)
    # Se establece la probabilidad de que la comida este arriba a la izquierda
    yFactor.setProbability({Y_POS_VAR: LEFT_TOP_VAL}, PROB_ONLY_LEFT_TOP)
    # Se establece la probabilidad de que la comida este abajo a la izquierda
    yFactor.setProbability({Y_POS_VAR: LEFT_BOTTOM_VAL}, PROB_ONLY_LEFT_BOTTOM)

    # Se agrega la tabla de probabilidad condicional a la red bayesiana
    bayesNet.setCPT(Y_POS_VAR, yFactor)

def fillHouseCPT(bayesNet, gameState):
    foodHouseFactor = bn.Factor([FOOD_HOUSE_VAR], [X_POS_VAR, Y_POS_VAR], bayesNet.variableDomainsDict())
    for assignment in foodHouseFactor.getAllPossibleAssignmentDicts():
        left = assignment[X_POS_VAR] == FOOD_LEFT_VAL
        top = assignment[Y_POS_VAR] == BOTH_TOP_VAL or \
                (left and assignment[Y_POS_VAR] == LEFT_TOP_VAL)

        if top and left and assignment[FOOD_HOUSE_VAR] == TOP_LEFT_VAL or \
                top and not left and assignment[FOOD_HOUSE_VAR] == TOP_RIGHT_VAL or \
                not top and left and assignment[FOOD_HOUSE_VAR] == BOTTOM_LEFT_VAL or \
                not top and not left and assignment[FOOD_HOUSE_VAR] == BOTTOM_RIGHT_VAL:
            prob = 1
        else:
            prob = 0

        foodHouseFactor.setProbability(assignment, prob)
    bayesNet.setCPT(FOOD_HOUSE_VAR, foodHouseFactor)

    ghostHouseFactor = bn.Factor([GHOST_HOUSE_VAR], [X_POS_VAR, Y_POS_VAR], bayesNet.variableDomainsDict())
    for assignment in ghostHouseFactor.getAllPossibleAssignmentDicts():
        left = assignment[X_POS_VAR] == GHOST_LEFT_VAL
        top = assignment[Y_POS_VAR] == BOTH_TOP_VAL or \
                (left and assignment[Y_POS_VAR] == LEFT_TOP_VAL)

        if top and left and assignment[GHOST_HOUSE_VAR] == TOP_LEFT_VAL or \
                top and not left and assignment[GHOST_HOUSE_VAR] == TOP_RIGHT_VAL or \
                not top and left and assignment[GHOST_HOUSE_VAR] == BOTTOM_LEFT_VAL or \
                not top and not left and assignment[GHOST_HOUSE_VAR] == BOTTOM_RIGHT_VAL:
            prob = 1
        else:
            prob = 0

        ghostHouseFactor.setProbability(assignment, prob)
    bayesNet.setCPT(GHOST_HOUSE_VAR, ghostHouseFactor)
    """
    #A
    # Se obtiene la distribucion de probabilidad de la casa de la comida y de la casa de los fantasmas
    q = inference.inferenceByVariableElimination(self.bayesNet, [FOOD_HOUSE_VAR, GHOST_HOUSE_VAR],
                                                    evidence, eliminationOrder)
    # Se obtiene la probabilidad de que la casa de la comida este arriba a la izquierda y la casa de los fantasmas este arriba a la derecha
    e1 = evidence.copy()
    # Se actualiza la evidencia con la posicion de la casa de la comida y de la casa de los fantasmas
    e1.update({FOOD_HOUSE_VAR: TOP_LEFT_VAL, GHOST_HOUSE_VAR: TOP_RIGHT_VAL})
    # Se obtiene la probabilidad de que la casa de la comida este arriba a la derecha y 
    # la casa de los fantasmas este arriba a la izquierda
    e2 = evidence.copy()
    e2.update({FOOD_HOUSE_VAR: TOP_RIGHT_VAL, GHOST_HOUSE_VAR: TOP_LEFT_VAL})
    # Se obtiene la probabilidad de que la casa de la comida este arriba a la izquierda y
    p1 = q.getProbability(e1)
    # la casa de los fantasmas este arriba a la derecha
    p2 = q.getProbability(e2)
    # Se obtiene la probabilidad de que la casa de la comida este arriba a la derecha y
    # la casa de los fantasmas este arriba a la izquierda
    leftExpectedValue = p1 * WON_GAME_REWARD + (1 - p1) * GHOST_COLLISION_REWARD
    # Se obtiene la probabilidad de que la casa de la comida este arriba a la izquierda y
    # la casa de los fantasmas este arriba a la derecha
    rightExpectedValue =  p2 * WON_GAME_REWARD + (1 - p2) * GHOST_COLLISION_REWARD

    return leftExpectedValue, rightExpectedValue


    #B
    # Se obtienen las probabilidades de explorar y los resultados de explorar
    for p, e in self.getExplorationProbsAndOutcomes(evidence):
        # t es la evidencia actualizada con la evidencia de explorar
        t = evidence.copy()s
        # Se actualiza la evidencia con la evidencia de explorar
        t.update(e)
        # Guardamos en expectedValue la probabilidad de explorar por la probabilidad
        #  de entrar en la casa con mayor probabilidad
        expectedValue += p * max(self.computeEnterValues(t, enterEliminationOrder))

    return expectedValue
"""

def fillObsCPT(bayesNet, gameState):
    """
    This funcion fills the CPT that gives the probability of an observation in each square,
    given the locations of the food and ghost houses.

    This function creates a new factor for *each* of 4*7 = 28 observation
    variables. Don't forget to call bayesNet.setCPT for each factor you create.

    The XXXPos variables at the beginning of this method contain the (x, y)
    coordinates of each possible house location.

    IMPORTANT:
    Because of the particular choice of probabilities higher up in the Bayes
    net, it will never be the case that the ghost house and the food house are
    in the same place. However, the CPT for observations must still include a
    vaild probability distribution for this case. To conform with the
    autograder, this function uses the *food house distribution* over colors when both the food
    house and ghost house are assigned to the same cell.
    """

    bottomLeftPos, topLeftPos, bottomRightPos, topRightPos = gameState.getPossibleHouses()

    #convert coordinates to values (strings)
    coordToString = {
        bottomLeftPos: BOTTOM_LEFT_VAL,
        topLeftPos: TOP_LEFT_VAL,
        bottomRightPos: BOTTOM_RIGHT_VAL,
        topRightPos: TOP_RIGHT_VAL
    }

    for housePos in gameState.getPossibleHouses():
        for obsPos in gameState.getHouseWalls(housePos):

            obsVar = OBS_VAR_TEMPLATE % obsPos
            newObsFactor = bn.Factor([obsVar], [GHOST_HOUSE_VAR, FOOD_HOUSE_VAR], bayesNet.variableDomainsDict())
            assignments = newObsFactor.getAllPossibleAssignmentDicts()

            for assignment in assignments:
                houseVal = coordToString[housePos]
                ghostHouseVal = assignment[GHOST_HOUSE_VAR]
                foodHouseVal = assignment[FOOD_HOUSE_VAR]

                if houseVal != ghostHouseVal and houseVal != foodHouseVal:
                    newObsFactor.setProbability({
                        obsVar: RED_OBS_VAL,
                        GHOST_HOUSE_VAR: ghostHouseVal,
                        FOOD_HOUSE_VAR: foodHouseVal}, 0)
                    newObsFactor.setProbability({
                        obsVar: BLUE_OBS_VAL,
                        GHOST_HOUSE_VAR: ghostHouseVal,
                        FOOD_HOUSE_VAR: foodHouseVal}, 0)
                    newObsFactor.setProbability({
                        obsVar: NO_OBS_VAL,
                        GHOST_HOUSE_VAR: ghostHouseVal,
                        FOOD_HOUSE_VAR: foodHouseVal}, 1)
                else:
                    if houseVal == ghostHouseVal and houseVal == foodHouseVal:
                        prob_red = PROB_FOOD_RED
                    elif houseVal == ghostHouseVal:
                        prob_red = PROB_GHOST_RED
                    elif houseVal == foodHouseVal:
                        prob_red = PROB_FOOD_RED

                    prob_blue = 1 - prob_red

                    newObsFactor.setProbability({
                        obsVar: RED_OBS_VAL,
                        GHOST_HOUSE_VAR: ghostHouseVal,
                        FOOD_HOUSE_VAR: foodHouseVal}, prob_red)
                    newObsFactor.setProbability({
                        obsVar: BLUE_OBS_VAL,
                        GHOST_HOUSE_VAR: ghostHouseVal,
                        FOOD_HOUSE_VAR: foodHouseVal}, prob_blue)
                    newObsFactor.setProbability({
                        obsVar: NO_OBS_VAL,
                        GHOST_HOUSE_VAR: ghostHouseVal,
                        FOOD_HOUSE_VAR: foodHouseVal}, 0)

            bayesNet.setCPT(obsVar, newObsFactor)

def getMostLikelyFoodHousePosition(evidence, bayesNet, eliminationOrder):
    """
    Question 7: Marginal inference for pacman

    Find the most probable position for the food house.
    First, call the variable elimination method you just implemented to obtain
    p(FoodHouse | everything else). Then, inspect the resulting probability
    distribution to find the most probable location of the food house. Return
    this.

    (This should be a very short method.)
    """
    "*** YOUR CODE HERE ***"
    # Se obtiene la distribucion de probabilidad de la casa de la comida
    res = inference.inferenceByVariableElimination(bayesNet, FOOD_HOUSE_VAR, evidence, eliminationOrder)
    m = 0
    #Con este for se obtiene la posicion de la casa de la comida con la mayor probabilidad
    for assignment in res.getAllPossibleAssignmentDicts():
        # Con el if comprobamos si la probabilidad de la casa de la comida es mayor que m, 
        # siendo m la probabilidad de que la casa de la comida este en la posicion que se esta comprobando
        if res.getProbability(assignment) > m:
            m = res.getProbability(assignment)
            # Se devuelve la posicion de la casa de la comida con la mayor probabilidad
            pos = assignment[FOOD_HOUSE_VAR]
    return {FOOD_HOUSE_VAR: pos}
    "*** END YOUR CODE HERE ***"

class BayesAgent(game.Agent):

    def registerInitialState(self, gameState):
        self.bayesNet, self.obsVars = constructBayesNet(gameState)
        fillCPTs(self.bayesNet, gameState)

        self.distances = cacheDistances(gameState)
        self.visited = set()
        self.steps = 0

    def getAction(self, gameState):
        self.visited.add(gameState.getPacmanPosition())
        self.steps += 1

        if self.steps < 40:
            return self.getRandomAction(gameState)
        else:
            return self.goToBest(gameState)

    def getRandomAction(self, gameState):
        legal = list(gameState.getLegalActions())
        legal.remove(Directions.STOP)
        random.shuffle(legal)
        successors = [gameState.generatePacmanSuccessor(a).getPacmanPosition() for a in legal]
        ls = [(a, s) for a, s in zip(legal, successors) if s not in gameState.getPossibleHouses()]
        ls.sort(key=lambda p: p[1] in self.visited)
        return ls[0][0]

    def getEvidence(self, gameState):
        evidence = {}
        for ePos, eColor in gameState.getEvidence().items():
            obsVar = OBS_VAR_TEMPLATE % ePos
            obsVal = {
                "B": BLUE_OBS_VAL,
                "R": RED_OBS_VAL,
                " ": NO_OBS_VAL
            }[eColor]
            evidence[obsVar] = obsVal
        return evidence

    def goToBest(self, gameState):
        evidence = self.getEvidence(gameState)
        unknownVars = [o for o in self.obsVars if o not in evidence]
        eliminationOrder = unknownVars + [X_POS_VAR, Y_POS_VAR, GHOST_HOUSE_VAR]
        bestFoodAssignment = getMostLikelyFoodHousePosition(evidence, 
                self.bayesNet, eliminationOrder)

        tx, ty = dict(
            zip([BOTTOM_LEFT_VAL, TOP_LEFT_VAL, BOTTOM_RIGHT_VAL, TOP_RIGHT_VAL],
                gameState.getPossibleHouses()))[bestFoodAssignment[FOOD_HOUSE_VAR]]
        bestAction = None
        bestDist = float("inf")
        for action in gameState.getLegalActions():
            succ = gameState.generatePacmanSuccessor(action)
            nextPos = succ.getPacmanPosition()
            dist = self.distances[nextPos, (tx, ty)]
            if dist < bestDist:
                bestDist = dist
                bestAction = action
        return bestAction

class VPIAgent(BayesAgent):

    def __init__(self):
        BayesAgent.__init__(self)
        self.behavior = None
        NORTH = Directions.NORTH
        SOUTH = Directions.SOUTH
        EAST = Directions.EAST
        WEST = Directions.WEST
        self.exploreActionsRemaining = \
                list(reversed([NORTH, NORTH, NORTH, NORTH, EAST, EAST, EAST,
                    EAST, SOUTH, SOUTH, SOUTH, SOUTH, WEST, WEST, WEST, WEST]))

    def reveal(self, gameState):
        bottomLeftPos, topLeftPos, bottomRightPos, topRightPos = \
                gameState.getPossibleHouses()
        for housePos in [bottomLeftPos, topLeftPos, bottomRightPos]:
            for ox, oy in gameState.getHouseWalls(housePos):
                gameState.data.observedPositions[ox][oy] = True

    def computeEnterValues(self, evidence, eliminationOrder):
        """
        Question 8a: Value of perfect information

        Given the evidence, compute the value of entering the left and right
        houses immediately. You can do this by obtaining the joint distribution
        over the food and ghost house positions using your inference procedure.
        The reward associated with entering each house is given in the *_REWARD
        variables at the top of the file.

        *Do not* take into account the "time elapsed" cost of traveling to each
        of the houses---this is calculated elsewhere in the code.
        """

        leftExpectedValue = 0
        rightExpectedValue = 0

        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()
        "*** END YOUR CODE HERE ***"

        return leftExpectedValue, rightExpectedValue

    def getExplorationProbsAndOutcomes(self, evidence):
        unknownVars = [o for o in self.obsVars if o not in evidence]
        assert len(unknownVars) == 7
        assert len(set(evidence.keys()) & set(unknownVars)) == 0
        firstUnk = unknownVars[0]
        restUnk = unknownVars[1:]

        unknownVars = [o for o in self.obsVars if o not in evidence]
        eliminationOrder = unknownVars + [X_POS_VAR, Y_POS_VAR]
        houseMarginals = inference.inferenceByVariableElimination(self.bayesNet,
                [FOOD_HOUSE_VAR, GHOST_HOUSE_VAR], evidence, eliminationOrder)

        probs = [0 for i in range(8)]
        outcomes = []
        for nRed in range(8):
            outcomeVals = [RED_OBS_VAL] * nRed + [BLUE_OBS_VAL] * (7 - nRed)
            outcomeEvidence = dict(zip(unknownVars, outcomeVals))
            outcomeEvidence.update(evidence)
            outcomes.append(outcomeEvidence)

        for foodHouseVal, ghostHouseVal in [(TOP_LEFT_VAL, TOP_RIGHT_VAL),
                (TOP_RIGHT_VAL, TOP_LEFT_VAL)]:

            condEvidence = dict(evidence)
            condEvidence.update({FOOD_HOUSE_VAR: foodHouseVal, 
                GHOST_HOUSE_VAR: ghostHouseVal})
            assignmentProb = houseMarginals.getProbability(condEvidence)

            oneObsMarginal = inference.inferenceByVariableElimination(self.bayesNet,
                    [firstUnk], condEvidence, restUnk + [X_POS_VAR, Y_POS_VAR])

            assignment = oneObsMarginal.getAllPossibleAssignmentDicts()[0]
            assignment[firstUnk] = RED_OBS_VAL
            redProb = oneObsMarginal.getProbability(assignment)

            for nRed in range(8):
                outcomeProb = combinations(7, nRed) * \
                        redProb ** nRed * (1 - redProb) ** (7 - nRed)
                outcomeProb *= assignmentProb
                probs[nRed] += outcomeProb

        return list(zip(probs, outcomes))

    def computeExploreValue(self, evidence, enterEliminationOrder):
        """
        Question 8b: Value of perfect information

        Compute the expected value of first exploring the remaining unseen
        house, and then entering the house with highest expected value.

        The method `getExplorationProbsAndOutcomes` returns pairs of the form
        (prob, explorationEvidence), where `evidence` is a new evidence
        dictionary with all of the missing observations filled in, and `prob` is
        the probability of that set of observations occurring.

        You can use getExplorationProbsAndOutcomes to
        determine the expected value of acting with this extra evidence.
        """

        expectedValue = 0

        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()
        "*** END YOUR CODE HERE ***"

        return expectedValue

    def getAction(self, gameState):

        if self.behavior == None:
            self.reveal(gameState)
            evidence = self.getEvidence(gameState)
            unknownVars = [o for o in self.obsVars if o not in evidence]
            enterEliminationOrder = unknownVars + [X_POS_VAR, Y_POS_VAR]
            exploreEliminationOrder = [X_POS_VAR, Y_POS_VAR]

            print(evidence)
            print(enterEliminationOrder)
            print(exploreEliminationOrder)
            enterLeftValue, enterRightValue = \
                    self.computeEnterValues(evidence, enterEliminationOrder)
            exploreValue = self.computeExploreValue(evidence,
                    exploreEliminationOrder)

            # TODO double-check
            enterLeftValue -= 4
            enterRightValue -= 4
            exploreValue -= 20

            bestValue = max(enterLeftValue, enterRightValue, exploreValue)
            if bestValue == enterLeftValue:
                self.behavior = ENTER_LEFT
            elif bestValue == enterRightValue:
                self.behavior = ENTER_RIGHT
            else:
                self.behavior = EXPLORE

            # pause 1 turn to reveal the visible parts of the map
            return Directions.STOP

        if self.behavior == ENTER_LEFT:
            return self.enterAction(gameState, left=True)
        elif self.behavior == ENTER_RIGHT:
            return self.enterAction(gameState, left=False)
        else:
            return self.exploreAction(gameState)

    def enterAction(self, gameState, left=True):
        bottomLeftPos, topLeftPos, bottomRightPos, topRightPos = \
                gameState.getPossibleHouses()

        dest = topLeftPos if left else topRightPos

        actions = gameState.getLegalActions()
        neighbors = [gameState.generatePacmanSuccessor(a) for a in actions]
        neighborStates = [s.getPacmanPosition() for s in neighbors]
        best = min(zip(actions, neighborStates), 
                key=lambda x: self.distances[x[1], dest])
        return best[0]

    def exploreAction(self, gameState):
        if self.exploreActionsRemaining:
            return self.exploreActionsRemaining.pop()

        evidence = self.getEvidence(gameState)
        enterLeftValue, enterRightValue = self.computeEnterValues(evidence,
                [X_POS_VAR, Y_POS_VAR])

        if enterLeftValue > enterRightValue:
            self.behavior = ENTER_LEFT
            return self.enterAction(gameState, left=True)
        else:
            self.behavior = ENTER_RIGHT
            return self.enterAction(gameState, left=False)

def cacheDistances(state):
    width, height = state.data.layout.width, state.data.layout.height
    states = [(x, y) for x in range(width) for y in range(height)]
    walls = state.getWalls().asList() + state.data.layout.redWalls.asList() + state.data.layout.blueWalls.asList()
    states = [s for s in states if s not in walls]
    distances = {}
    for i in states:
        for j in states:
            if i == j:
                distances[i, j] = 0
            elif util.manhattanDistance(i, j) == 1:
                distances[i, j] = 1
            else:
                distances[i, j] = 999999
    for k in states:
        for i in states:
            for j in states:
                if distances[i,j] > distances[i,k] + distances[k,j]:
                    distances[i,j] = distances[i,k] + distances[k,j]

    return distances

# http://stackoverflow.com/questions/4941753/is-there-a-math-ncr-function-in-python
def combinations(n, r):
    r = min(r, n-r)
    if r == 0: return 1
    numer = functools.reduce(op.mul, range(n, n-r, -1))
    denom = functools.reduce(op.mul, range(1, r+1))
    return numer / denom


if __name__ == "__main__":
    constructHeartAttackBayesNet()